import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class DoubleroomTest2 {

    Doubleroom doubleroom;
    int Integer;
    @Test
    public void TestContactDetails(){
        Doubleroom doubleroom = new Doubleroom("Rajesh","9494021995","M","AbC","123456789","M");

        assertAll(
                () -> assertTrue(doubleroom.contact.length() == 10),
                () -> assertFalse(doubleroom.contact.length()!= 10,"Enter a valid number"),
                () ->  assertNotEquals(doubleroom.contact.getClass().getSimpleName(), ((Object) Integer).getClass().getSimpleName(), "Type of contact must be Integer"),
                ()->  assertTrue(!doubleroom.name.isEmpty()),
                ()->  assertFalse(doubleroom.name.isEmpty())
        );

    }

    @Test
    public void testDoubleroom() {
        Doubleroom doubleroom = new Doubleroom("Rajesh","9494021995","M","AbC","123456789","M");

        Food order=  new Food(1,2);
        doubleroom.food.add(order);
//        System.out.println(order.price);
        assertEquals("Rajesh",doubleroom.name);
    }

    @Test
    public void nullTest(){
        assertNull(doubleroom,"When no details filles it is null");
    }

    @Test
    public void NotNullTest(){
        Doubleroom doubleroom = new Doubleroom("Rajesh","9494021995","M","AbC","123456789","M");
        assertNotNull(doubleroom,"When details filles it is not null");
    }

    @Test
    public void WhenDetailsMissing(){
        Doubleroom doubleroom = new Doubleroom("Rajesh",null,"M","AbC","123456789","M");
        assertEquals(null,doubleroom.contact);
    }




}