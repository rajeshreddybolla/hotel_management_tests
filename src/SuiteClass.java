import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        FoodTest.class,
        SingleroomTest.class,
        DoubleroomTest2.class,
        holderTest.class,
        HotelTest.class
})
public class SuiteClass {

    @BeforeAll
   public void PrintBeoreAll(){
        System.out.println("Starting test for all classes");
    }

    @Before
    public void printBeforeach(){
        System.out.println("started a test for class");
    }

    @After
    public void printAfterach(){
        System.out.println("completed test for class");
    }

    @AfterAll
    public void printAfterAll() {
        System.out.println("completed test for all classes");
    }
}
