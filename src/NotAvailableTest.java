import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class NotAvailableTest {

    NotAvailable notAvailable = new NotAvailable();

    @Test
    public void testNotAvailable(){
        assertEquals("Not Available !", notAvailable.toString());
    }

    @Test
    public void NullTest(){
        assertNotNull(notAvailable.toString());
    }



}