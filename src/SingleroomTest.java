import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class SingleroomTest {
    Singleroom singleroom;
    int n ;
    @Test
    public void testSingleroom(){
        Singleroom singleroom = new Singleroom("Rajesh","123","M");
        assertEquals("Rajesh",singleroom.name );
    }

    @Test
    public void nullTest(){
        assertNull(singleroom,"When no details filles it is null");
    }

    @Test
    public void NotNullTest(){
        Singleroom singleroom = new Singleroom("Rajesh","123","M");
//        assertEquals("Rajesh",singleroom.name );
        assertNotNull(singleroom,"When details filles it is not null");
    }

    @Test
    public void WhenDetailsMissing(){
        Singleroom singleroom = new Singleroom("Rajesh", null,"M");
        assertEquals(null,singleroom.contact);
    }


    @Test

    public void TestContactDetails(){
        Singleroom singleroom = new Singleroom("Rajesh", "9494021995","M");

        assertAll(
                () -> assertTrue(singleroom.contact.length() == 10),
                () -> assertFalse(singleroom.contact.length()!= 10,"Enter a valid number"),
                () ->  assertNotEquals(singleroom.contact.getClass().getSimpleName(), ((Object) n).getClass().getSimpleName(), "Type of contact must be Integer")
        );

    }



}