import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import static org.junit.jupiter.api.Assertions.*;

public class FoodTest {



    @Test
    public void TestFood() {
        Food actual = new Food(1,2);
        int expected = 100;
        Assertions.assertEquals(100, actual.price);
    }

    @Test
    public void TestFood2() {
        Food actual = new Food(1,1);
        int expected = 100;
        Assertions.assertEquals(50, actual.price);
    }

    @Test
    public void TestFood3() {
        Food actual = new Food(1 ,1);
        assertNotNull(actual.price);
    }
}