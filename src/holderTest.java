import org.junit.Test;

import javax.xml.ws.Holder;

import static org.junit.jupiter.api.Assertions.*;

public class holderTest {

    holder Holder = new holder();
    @Test
    public void testluxury_doublerrom(){
        int actualNoOFRooms  = Holder.luxury_doublerrom.length;
        int expectedNoOFRooms = 10;
        assertAll(
                () -> assertEquals(expectedNoOFRooms,actualNoOFRooms,"Length is fine"),
                () ->  assertNotNull(Holder.luxury_doublerrom,"Not null"),
                () ->  assertTrue(expectedNoOFRooms == actualNoOFRooms)
        );
    }

    @Test
    public void testluxury_singleerrom(){
        int actualNoOFRooms= Holder.luxury_singleerrom.length;
        int expectedNoOFRooms = 10;
        assertSame(expectedNoOFRooms,actualNoOFRooms,"Length is fine");

    }

    @Test
    public void testdeluxe_doubleroom(){
        int actualNoOFRooms = Holder.deluxe_doublerrom.length;
        int expectedNoOFRooms= 20;
        assertSame(expectedNoOFRooms,actualNoOFRooms,"Length is fine");

    }



    @Test
    public void testdeluxe_singleroom(){
        int actualNoOFRooms = Holder.deluxe_singleerrom.length;
        int expectedNoOFRooms = 20;
        assertAll(
                () -> assertSame(expectedNoOFRooms,actualNoOFRooms,"Length is fine"),
                () -> assertNotNull(Holder.deluxe_singleerrom,"Not null")
        );
    }



}